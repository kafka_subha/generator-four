package com.subha.kafka.processor;

import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by subha on 03/07/2018.
 */

@Component("genFour")
public class GeneratorFourProcessor {

    private static final Logger LOG = LoggerFactory.getLogger(GeneratorFourProcessor.class);

    public void process(Exchange exchange) {
        int seed = Integer.parseInt(exchange.getIn().getBody().toString().split("-")[0]);
        long generatedResult = (seed*(seed + 1))/2;

        LOG.info("Got seed " + seed);
        LOG.info("Generator one generated number " + generatedResult);
        exchange.getIn().setBody(generatedResult + "-GEN4-" + exchange.getIn().getBody().toString().split("-")[1]);
    }

}
